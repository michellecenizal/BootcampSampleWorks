<!DOCTYPE html>
<html>
<head>
	<title>Arrays</title>
</head>
<body>
	<h2>Type of Arrays</h2>

	<?php

	

		// $colors = ['red', 'blue', 'green', 'orange', 'indigo'];
		// // echo  count($colors); 



		// $arrlength = count($colors);

		// for ($x = 0; $x < $arrlength; $x+=2) {
		// 	echo $colors[$x];
		// }

	// $ages = ['Peter' => '35', 'Ben' => '35', 'Joe' => '35'];
	// // key => value

	// foreach($ages as $patientName => $age) {  //foreach (array as key => value)
	// 	echo 'Key=' . $patientName . " , Value= " . $age; 
	// 	"<br>";
	// }

	// Associative Arrays

	// $myteam = ['Durant' => '35', 'Curry' => '30', 'Thompson' => '11'];

	// 	foreach ($myteam as $ballName => $jerseyNum) {
	// 		echo 'SuperstarName' .  $ballName . ", JerseyNumber = " . $jerseyNum;
	// 	}

		
		// reverse sort - and value sorting

		// $rainbow = ['red', 'blue', 'green', 'orange', 'indigo'];
		// sort($rainbow);
		// foreach ($rainbow as $color) {
		// 	echo $color . "<br>";
		// }

		// rsort($rainbow);
		// foreach($rainbow as $color) {
		// 	echo $color . "<br>";
		// }
		// asort($rainbow);
		// foreach($rainbow as $color) {
		// 	echo $color . "<br>";
		// }
		// arsort($rainbow);
		// foreach($rainbow as $color) {
		// 	echo $color . "<br>";
		// }
		// krsort($rainbow);
		// foreach($rainbow as $color) {
		// 	echo $color . "<br>";
		// }
		// ksort($rainbow);
		// foreach($rainbow as $color) {
		// 	echo $color . "<br>";
		// }

		// Multi - Dimensional Array


		// $team_ironman = ['Tony', 'War Machine', 'Vision', 'Starke'];
		// $team_cap = ['Cap America', 'Bucky', 'Hawkeye', 'Falcon'];
		// $civil_war = [$team_ironman, $team_cap];


		$stud1 = ['name' => 'Kynt', 'age' => 21, 'occupation' =>'RTP'];
		$stud2 = ['name' => 'Sian', 'age' => 18, 'occupation' =>['Student', 'lover']];

		$b8dc = [$stud1, $stud2];
		echo $b8dc [1] ['name'] . " is a " .  $b8dc [1]['occupation'][1];

	?>

</body>
</html>