<?php
	session_start(); // to make use of the session
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,intial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
	<title>Login page</title>
</head>
<body>

<div class="container">

	<form class="form-signin" action = "authenticate.php" method = "POST">
		<h1 class="form-signin-heading text-muted">Sign In</h1>
		<?php
			if (isset($_SESSION['message']) && !empty($_SESSION['message'])) { // if isset ---> exists and NOT empty
				echo $_SESSION['message'];
				session_unset();
				session_destroy();
			}
		?>
		<input type="text" class="form-control" placeholder="Email address" required="" autofocus="" name = "username" id="usrnm" >
		<input type="password" class="form-control" placeholder="Password" required="" name = "password" id="pwd">
		<button class="btn btn-lg btn-primary btn-block" type="submit" id="mybutton" >Sign In</button>
		</form>

		<div id = "jsonsection"></div>
		<div id="outputsection"></div>

		<script>

		$(document).ready(function(){
			$('#mybutton').submit(function(){
				var usrnm = $('#usrnm'). val();	
				var pwd = $('#pwd'). val();	
				$.ajax(
				{
					'url':'userlist.php', 
					'data':{'$admin':myinput}, 
					'type':'GET', 
					'success':editHTML, 
				}

				);
			});		
		});

		function editHTML(jsondata) { 
			$('#jsonsection').html('Received:' + jsondata);

			if(jsondata != ""){
				var data = JSON.parse(jsondata);

				var htmlstr = "<hr>";
				htmlstr += "username:" + data.username + "<br>";
				htmlstr += "Password:" + data.password +"<br>";
				$("#outputsection").html(htmlstr);

			} else {
				$('#outputsection'.html("<hr> Account not found"));
			}
		}
	</script>


	

</div>
</body>
</html>