<!DOCTYPE html>
<html>
<head>
	<title>Authenticate</title>
</head>
<body>

		<?php
			session_start();
			include_once "userlist.php";
			$username = htmlspecialchars($_POST['username']);
			$password = htmlspecialchars($_POST['password']);

			if(authenticate($username,$password)) {
				$_SESSION['user'] = $username;
				header ('Location:home1.php'); // moves to the page specified in location
			} else {
				$_SESSION['message'] = 'Please login first';
				header('Location: login.php');
			}

			function authenticate($username,$password) {
				if($username == 'admin' || $username == "admin@mail.com" && $password == 'secret') {
					return true;
				}else {
					return false;
				}
			}
		?>
</body>
</html>