<?php
	session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<?php
		if(isset($_SESSION['user'])) {
			echo "<h1> Hello " . $_SESSION['user'] . "</h>";
		} else {
			echo "No logged in user found.";
		}
	?>

</body>
</html>