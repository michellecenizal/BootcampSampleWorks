<!DOCTYPE html>
<html>
<head>
	<title> For loops</title>
</head>
<body>

<?php

$a = 1;
$b= ++$a; 
echo $a;
echo $b;

$c = 1;
$d = $c++;
echo $c;
echo $d;

	// for($count = 1; $count <= 12; ++$count) {
	// 	echo "$count times 12 is " . count * 12 . "<br>";
	// }
?>

</body>
</html>